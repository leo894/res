package com.example.reservac;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;





public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    ListView listado;
    TextView reservados;
    EditText Fecha,Hora,cedula_txt, nombre_txt,servicio_txt;
    Button agendar;
    private  int dia,mes,ano,hora,minutos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Fecha=findViewById(R.id.fecha_txt);
        reservados=findViewById(R.id.reservados_txt);
        Hora=findViewById(R.id.hora_txt);
        cedula_txt = findViewById(R.id.cedula_txt);
        nombre_txt = findViewById(R.id.nombre_txt);
        servicio_txt=findViewById(R.id.servicio_txt);


        agendar=findViewById(R.id.agendar_btn);
        Fecha.setOnClickListener(this);
        Hora.setOnClickListener(this);
        reservados.setOnClickListener(this);


        agendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Book registro = new Book(servicio_txt.getText().toString(),cedula_txt.getText().toString(),nombre_txt.getText().toString()
                        ,Fecha.getText().toString(),Hora.getText().toString());
                registro.save();
                Toast.makeText(MainActivity.this,"Se guardo correctamente ", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onClick(View v) {
        if (v==Hora){
            final Calendar c= Calendar.getInstance();
            hora=c.get(Calendar.HOUR_OF_DAY);
            minutos=c.get(Calendar.MINUTE);

            TimePickerDialog timePickerDialog=new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    Hora.setText(hourOfDay+":"+minute);
                }
            },hora,minutos,false);
            timePickerDialog.show();
        }

        if(v==reservados){
            listado=(ListView)findViewById(R.id.Lista);
            List<Book> books = Book.listAll(Book.class);
            List<String> listaTextoLibros = new ArrayList<>();
            for (Book libro: books){
                Log.e("Libro"
                        , libro.getFecha()+ ", " +libro.getHora()+ ", " +libro.getServicio() );
                listaTextoLibros.add(libro.getFecha()+ ", " +libro.getHora()+ ", " +libro.getServicio() );
            }
            ArrayAdapter<String> itemsAdapter = new ArrayAdapter <String> (
                    this,  android.R.layout.simple_list_item_1, listaTextoLibros);

            listado.setAdapter(itemsAdapter);
        }

        if (v==Fecha) {
            final Calendar c = Calendar.getInstance();
            dia = c.get(Calendar.DAY_OF_MONTH);
            mes = c.get(Calendar.MONTH);
            ano = c.get(Calendar.YEAR);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Fecha.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                }
            }
                    , dia, mes, ano);
            datePickerDialog.show();
        }
    }


}
