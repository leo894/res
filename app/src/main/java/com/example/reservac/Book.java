package com.example.reservac;

import com.orm.SugarRecord;

public class Book extends SugarRecord<Book> {

    String cedula,nombre,fecha,hora,servicio;

    public Book() {
    }

    public Book(String cedula, String nombre, String fecha, String hora, String servicio) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.fecha = fecha;
        this.hora = hora;
        this.servicio = servicio;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }
}
